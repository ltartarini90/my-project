# my-project
No code is provided due to lack of time. I just try to explain the design and implementation in here based on my experience and the design patterns I prefer.

## Question 1
`1. Write a Terraform configuration that will create the infrastructure needed for publishing a high scale microservice to the internet. Write the configuration for the cloud provider you're more familiar with.`

We are going to use EKS for this task.

To do this we need a to setup the networking part (VPCs, Subnets, peerings, Roure53, ...) which is generic for each k8s cluster we want to spin up, then we have the AWS module for an EKS cluster creation that helps us to create variuos cluster for each environment we need:
- stage
- qa
- production
- ...

We are going to use a S3 bucket as Terraform backend for our `.tfstate` files and a Terraform `map` variable to configure users' authN and authZ.

`2. Write a CI/CD pipeline to deploy a simple microservice maximizing the deployment frequency and minimizing the risk of service disruption. Write the configuration with any CI/CD tool you're more familiar with. (provide a link to your solution)`

We are going to use GitLab CI for this purpose. Each microservice will have its own Git repo managed via `Helm` or `Kustomize`. In each microservice repo we will have the `.gitlab-ci.yml` file in charge to describe the CICD pipeline which will have the following steps:
- `tag`: tagging the code release
- `requirements`: check requirements such as Terraform, Vault, ...
- `test`: testing the code
- `build`: build the source code
- `build-image`: build the Dockerfile image and upload it to ECR
- `deploy`: deployment into an environment

Depending on the k8s deployment tool you choose (`Helm` or `Kustomize`) the pipeline will have specific commands to properly build and deploy manifest `.yaml` files.

`3. Suppose that you need to release a new service in an application with 10 million users. Please describe how you would`

`design and sizing service architecture`

k8s HPA + EKS cluster autoscaler with default replicas set via SLIs and SLOs maybe gathered via Prometheus recording and alerting rules and viewed in Grafana dashboards.

Do performance tests and Chaos Engineering.

`release the new service`

Developers will release their own service based on the CICD platform we provided with Slack integration.

`minimize and detect issues in production`

ELK stack + APM tool such as Dynatrace.

`handle high traffic peaks, suppose that active users raise from 100 hundred to 5 millions in 1 minute`

AWS autoscaling for nodes, HPA for Pods, and proactive scaling by leveraging performance tests and APM analysis. 
